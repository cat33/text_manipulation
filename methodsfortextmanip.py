def count_men():
    count=0;
    with open('example_text.txt', 'r') as f:
        while (1):
            readlinein = f.readline()
            if readlinein != '':
                count = readlinein.count('men')+count
            else:
                print(count)
                break

def capitalize_women():
    newf=open('example_text_new.txt','w')
    with open('example_text.txt','r') as f:
        while(1):
            readlinein=f.readline()
            if readlinein !='':
                capitalizedstring=readlinein.replace('women','WOMEN')
                newf.write(capitalizedstring)
            else:
                break


def contains_blue_devil():
    count=0
    with open('example_text.txt','r') as f:
        while(1):
            readlinein = f.readline()
            if readlinein != '':
                if "Blue Devil" in readlinein:
                    count=count+1
            else:
                if count>0:
                    print('Blue Devil present')
                else:
                    print('Blue Devil not present')

                break

def find_non_terminal_said():
    count=0
    import re
    with open('example_text.txt','r') as f:
        while(1):
            readlinein=f.readline()
            if readlinein != '':
                q=re.compile('said')
                l=re.search(q,readlinein)
                print(l)
                if l!=None:
                    print(l.start())
                    print(readlinein[l.start()+5])

                #endofmatch=m.end()
                count=count+1
            else:
                print(count)
                break



if __name__ == '__main__':
    #capitalize_women()
    #count_men()
    #contains_blue_devil()
    find_non_terminal_said()


